Django==1.6.5
South==1.0
Pillow==2.5.1
django-cms==3.0.6
django-bootstrap3==4.8.2
cmsplugin-filer==0.9.9
django-admin-sortable2==0.3.1
django-filer==0.9.7
django-hvad==0.4.1
django-reversion==1.8.1
djangocms-text-ckeditor==2.1.6
-e git://github.com/h4/django-sociallinks.git@0.1.5#egg=sociallinks
-e git://github.com/h4/django-googleanalytics.git@0.1.0#egg=google_analytics
-e git://github.com/h4/djangocms-news.git@0.2.11#egg=news
django-compressor==1.4

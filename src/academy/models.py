# coding=utf-8
from cms.models import CMSPlugin
from django.db import models
from django.db.models.base import model_unpickle
from filer.fields.image import FilerImageField
from hvad.models import TranslatableModel, TranslatedFields
from django.utils.translation import ugettext_lazy as _


class Teacher(TranslatableModel):
    translations = TranslatedFields(
        first_name=models.CharField(_('first name'), max_length=255),
        middle_name=models.CharField(_('middle name'), max_length=255, null=True, blank=True),
        last_name=models.CharField(_('last name'), max_length=255),
        position=models.CharField(_('position'), max_length=255, null=True, blank=True),
    )
    photo = FilerImageField(verbose_name=_("photo"), null=True, blank=True)

    _sortable_fields = (
        ('first_name', _('first name')),
        ('last_name', _('last name')),
        ('position', _('position')),
    )

    @classmethod
    def get_sortable_fields(cls):
        return cls._sortable_fields

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")

    @property
    def full_name(self):
        f_name = self.lazy_translation_getter("first_name")
        l_name = self.lazy_translation_getter("last_name")
        m_name = self.lazy_translation_getter("middle_name")
        if m_name is None:
            return u"{0} {1}".format(f_name, l_name)
        else:
            return u"{0} {1} {2}".format(l_name, f_name, m_name)

    def __unicode__(self):
        return self.full_name


class TeachersPlugin(CMSPlugin):
    ordering_field = models.CharField(_('order by'), max_length=32, choices=Teacher.get_sortable_fields(), blank=True, null=True)
    reversed = models.BooleanField(_('reversed'), default=False)


class PartnerPlugin(CMSPlugin):
    title = models.CharField(_('title'), max_length=255)
    logo = FilerImageField(verbose_name=_('logotype'))
    link = models.URLField(_('link'), null=True, blank=True)

    def __unicode__(self):
        return self.title

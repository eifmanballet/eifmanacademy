# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from academy.models import TeachersPlugin, Teacher, PartnerPlugin
from django.utils.translation import ugettext_lazy as _


@plugin_pool.register_plugin
class Teachers(CMSPluginBase):
    name = _("Teachers")
    model = TeachersPlugin
    module = "Academy"
    render_template = "academy/plugins/teachers.html"

    def render(self, context, instance, placeholder):
        if instance.ordering_field is None:
            teachers = Teacher.objects.language().all()
        else:
            ordering = instance.ordering_field if not instance.reversed else '-{1}'.format(instance.ordering_field)
            teachers = Teacher.objects.language().order_by(ordering)


        objects = []
        i = 0
        while i < len(teachers):
            objects.append(teachers[i:i+4])
            i += 4

        context.update({
            'objects': objects,
            'instance': instance,
            'placeholder': placeholder,
        })

        return context


@plugin_pool.register_plugin
class Partner(CMSPluginBase):
    name = _("Partner")
    model = PartnerPlugin
    module = "Academy"
    render_template = "academy/plugins/partner.html"


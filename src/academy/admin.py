# coding=utf-8
from django.contrib import admin
from hvad.admin import TranslatableAdmin
from academy.models import Teacher

admin.site.register(Teacher, TranslatableAdmin)

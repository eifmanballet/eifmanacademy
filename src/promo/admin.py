# coding=utf-8
from django.contrib import admin
from hvad.admin import TranslatableAdmin, TranslatableTabularInline
from promo.models import Item, ItemText


class ItemTextInline(TranslatableTabularInline):
    model = ItemText
    extra = 0


class ItemAdmin(TranslatableAdmin):
    list_display = ['page_link', 'row', 'position',]
    list_editable = ['row', 'position',]
    ordering = ['row', 'position',]
    raw_id_fields = ('page_link', )
    inlines = [
        ItemTextInline
    ]

admin.site.register(Item, ItemAdmin)

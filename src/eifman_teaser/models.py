# coding=utf-8
from cms.models.fields import PageField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin
from cmsplugin_filer_image.models import ThumbnailOption
from ckeditor.fields import RichTextField as HTMLField
from filer.fields.image import FilerImageField


class EifmanTeaserPlugin(CMSPlugin):
    title = models.CharField(_("Title"), max_length=255)
    image = FilerImageField(blank=True, null=True, verbose_name=_("image"))
    link = PageField(verbose_name=_("link"), null=True, blank=True)
    thumbnail_option = models.ForeignKey(ThumbnailOption, null=True, blank=True, verbose_name=_("thumbnail option"),
                                        help_text=_('overrides width, height, crop and upscale with values from the selected thumbnail option'))
    width = models.PositiveIntegerField(_("width"), null=True, blank=True)
    height = models.PositiveIntegerField(_("height"), null=True, blank=True)
    crop = models.BooleanField(_("Crop"), default=False)
    use_autoscale = models.BooleanField(_("Auto scale"), default=False)

    description = HTMLField(_("Text"))

    def __unicode__(self):
        return self.title

# coding = utf-8
from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Mikhail Baranov', 'dev@brnv.ru',)
)

MANAGERS = (
    ('Empty', 'none@none.ru')
)

CONTACT_EMAILS = [
    'dev@brnv.ru',
]

ALLOWED_HOSTS = [
    '.eifmanacademy.ru',
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

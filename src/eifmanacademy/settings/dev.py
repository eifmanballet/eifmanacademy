# coding = utf-8
from .base import *

INSTALLED_APPS = INSTALLED_APPS + (
    'debug_toolbar',
)

CONTACT_EMAILS = [
    'dev@brnv.ru',
]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

COMPRESS_ENABLED = False

# coding=utf-8
from django.utils.translation import ugettext_lazy as _

gettext = lambda s: s

CMS_PLACEHOLDER_CONF = {
    'conception__intro_image': {
        'plugins': ['FilerImagePlugin',],
        'name': _('intro image'),
        'limits': {
            'FilerImagePlugin': 1,
        },
    },
    'conception__intro_text': {
        'plugins': ['TextPlugin'],
        'name': _('intro text'),
        'default_plugins':[
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body': '<p>Академия танца — творческая лаборатория, реализующая концепцию современного танца Бориса Яковлевича Эйфмана, является, с одной стороны, хранителем и творческим продолжателем великих традиций отечественного классического балетного искусства, с другой — школой мастерства для молодых артистов, экспериментальной площадкой, на которой отрабатываются инновационные методы подготовки и воспитания танцовщиков XXI века, владеющих разнообразными техниками танца, различными стилистическими и эстетическими направлениями современного хореографического искусства и достижениями современного спорта.</p>',
                },
            },
        ],
        'limits': {
            'TextPlugin': 1,
        },
    },
    'conception__mission_text': {
        'plugins': ['TextPlugin',],
        'name': _('missions of academy'),
        'default_plugins':[
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body': '<p>Описание миссии 1.</p>',
                },
            },
        ],
        'limits': {
            'TextPlugin': 1,
        },
        'plugin_labels': {
            'TextPlugin': _('Add a mission'),
        }
    },
    'conception-page__goals_text': {
        'plugins': ['TextPlugin',],
        'name': _('goals of academy'),
        'default_plugins': [
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body': '<p>Цель академии 1.</p>',
                },
            },
        ],
        'limits': {
            'TextPlugin': 3,
        },
    },
    'conception-page__tasks_images': {
        'plugins': ['FilerImagePlugin',],
        'name': _('images'),
        'limits': {
            'FilerImagePlugin': 3,
        },
    },
    'conception-page__tasks_text': {
        'plugins': ['TextPlugin',],
        'name': _('tasks of academy'),
        'default_plugins': [
            {
                'plugin_type': 'TextPlugin',
                'values': {
                    'body': '<p>Задачи академии.</p>',
                },
            },
        ],
        'limits': {
            'TextPlugin': 1,
        },
    },
    'enter__intro': {
        'plugins': ['TextPlugin',],
        'name': _('intro'),
        'limits': {
            'TextPlugin': 1,
        }
    },
    'enter__banner': {
        'plugins': ['FilerImagePlugin',],
        'name': _('banner'),
        'limits': {
            'FilerImagePlugin': 1,
        }
    },
    'enter__exams': {
        'plugins': ['CollapsibleBoxPlugin',],
        'name': _('exams'),
        'limits': {
            'CollapsedBoxPlugin': 1,
        }
    },
    'enter__phones': {
        'plugins': ['TextPlugin',],
        'name': _('phones'),
        'limits': {
            'TextPlugin': 1,
        }
    },
    'enter__comment': {
        'plugins': ['TextPlugin',],
        'name': _('comment'),
        'limits': {
            'TextPlugin': 1,
        }
    },
    'enter__applications': {
        'plugins': ['TextPlugin','ApplicationPlugin',],
        'name': _('applications'),
        'limits': {
            'TextPlugin': 1,
        }
    },
    'promo': {
        'plugins': ['CMSPromoBlock',],
        'name': _('promo'),
        'limits': {
            'CMSPromoBlock': 1,
        }
    },
}

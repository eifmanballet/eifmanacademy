# coding = utf-8
from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Mikhail Baranov', 'dev@brnv.ru',)
)

CONTACT_EMAILS = [
    'dev@brnv.ru',
]

MANAGERS = ADMINS

ALLOWED_HOSTS = [
    'eifmanacademy.h404.ru',
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

INTERNAL_IPS = [
    "127.0.0.1",
    "94.19.215.21",
    "178.130.45.219",
    "87.202.184.72",
    ]

# coding=utf-8
import os
from django.utils.translation import ugettext_lazy as _
from .cms_placeholders import CMS_PLACEHOLDER_CONF

gettext = lambda s: s

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

SECRET_KEY = os.getenv('SECRET_KEY')

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'south',
    'cms',
    'mptt',
    'sekizai',
    'menus',
    'adminsortable',

    'djangocms_text_ckeditor',
    'filer',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_image',
    'reversion',
    'easy_thumbnails',
    'compressor',

    'bootstrap3',
    'sociallinks',
    'google_analytics',
    'news',
    'hvad',
    'ckeditor',
    'genericadmin',

    'core',
    'academy',
    'bem',
    'contacts',
    'eifman_application',
    'eifman_box',
    'eifman_collapsible_box',
    'eifman_teaser',
    'eifman_gallery',
    'gallery',
    'promo',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',

    'cms.middleware.language.LanguageCookieMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings',
    'sekizai.context_processors.sekizai',
)


ROOT_URLCONF = 'eifmanacademy.urls'

WSGI_APPLICATION = 'eifmanacademy.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../databases/db.sqlite3'),
    }
}

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('ru', gettext('Russian')),
    ('en', gettext('English')),
)

CMS_LANGUAGES = {
    'default': {
        'hide_untranslated': False,
        'redirect_on_fallback': True,
        'public': True,
    },
    1: [
        {
            'redirect_on_fallback': True,
            'code': 'ru',
            'hide_untranslated': False,
            'name': gettext('ru'),
            'public': True,
        },
        {
            'redirect_on_fallback': True,
            'code': 'en',
            'hide_untranslated': False,
            'name': gettext('en'),
            'public': True,
        },
    ],
}

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, '../static/')

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, '../media/')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

CMS_TEMPLATES = (
    ('inner.html', _('Inner page')),
    ('index.html', _('Index page')),
    ('conception.html', _('Conception page')),
    ('enter.html', _('Enter page')),
)

EMAIL_SUBJECT_PREFIX = "[eifmanacademy.ru] "

DEFAULT_SENDER = "no-reply@eifmanballet.ru"

THUMBNAIL_HIGH_RESOLUTION = True

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Undo', 'Redo', '-', 'ShowBlocks'],
            ['Format', 'Styles'],
            ['TextColor', 'BGColor', '-', 'PasteText', 'PasteFromWord'],
            ['Maximize', ''],
            '/',
            ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', "Image", "Anchor", "Youtube"],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Table'],
            ['Source']
        ],
    }
}

CKEDITOR_UPLOAD_PATH = 'uploads/'

CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_CMS': [
        ['Undo', 'Redo'],
        ['cmsplugins', '-', 'ShowBlocks'],
        ['Format', 'Styles'],
        ['TextColor', 'BGColor', '-', 'PasteText', 'PasteFromWord'],
        ['Maximize', ''],
        '/',
        ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', "Image", "Anchor", "Youtube"],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Table'],
        ['Source']
    ],
    'toolbar': 'CMS',
    'skin': 'moono',
    'toolbarCanCollapse': False,
}

DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': None,
}

CMSPLUGIN_FILER_IMAGE_STYLE_CHOICES = (
    ('default', 'Default'),
    ('teacher', 'Teacher'),
    ('teacher-centered', 'Teacher centered'),
)

BOOTSTRAP3 = {
    'jquery_url': '{}/js/jquery-1.11.1.min.js'.format(STATIC_URL),
    'css_url': '{}/css/bootstrap.css'.format(STATIC_URL),
    'base_url': STATIC_URL,
}

NEWS_RELATED_MODELS = ('eifman_gallery/album', )

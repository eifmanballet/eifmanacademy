# coding=utf-8
from classytags.helpers import InclusionTag
from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.tag()
class bem_core(InclusionTag):
    template = 'bem/bem-core.html'

    def get_context(self, context, **kwargs):
        return context

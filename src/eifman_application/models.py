# coding=utf-8
from django.db import models
from cms.models import CMSPlugin
from django.utils.translation import ugettext_lazy as _


class Application(CMSPlugin):
    title = models.CharField(_("title"), max_length=255)
    link = models.URLField(_("link to application"), max_length=512)

    def __unicode__(self):
        return self.title

# coding=utf-8
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from eifman_application.models import Application


@plugin_pool.register_plugin
class ApplicationPlugin(CMSPluginBase):
    model = Application
    module = 'Academy'
    name = _("Link to application")
    render_template = "eifman_application/application.html"

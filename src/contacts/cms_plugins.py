# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from contacts.models import AddressPlugin
from django.utils.translation import ugettext_lazy as _


@plugin_pool.register_plugin
class CMSAddress(CMSPluginBase):
    name = _("Address")
    model = AddressPlugin
    module = "Academy"
    render_template = "contacts/plugins/address.html"

    def render(self, context, instance, placeholder):
        address = instance.address

        context.update({
            'object': address,
            'instance': instance,
            'placeholder': placeholder,
        })

        return context

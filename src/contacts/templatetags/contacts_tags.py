# coding=utf-8
from classytags.helpers import InclusionTag
from django import template
from django.template.loader import render_to_string
from contacts.models import Address, Officials

register = template.Library()


@register.inclusion_tag('contacts/plugins/address.html')
def address():
    address_object = Address.objects.get()

    return {
        "object": address_object
    }

@register.inclusion_tag('contacts/plugins/officials.html')
def officials():
    objects = Officials.objects.all()

    return {
        "objects": objects
    }


class address_map(InclusionTag):
    template = 'contacts/plugins/map.html'

    def get_context(self, context, **kwargs):
        address_object = Address.objects.get()
        context['object'] = address_object
        return context

    def render_tag(self, context, **kwargs):
        data = self.get_context(context)
        output = render_to_string(self.template, data)
        return output

register.tag(address_map)

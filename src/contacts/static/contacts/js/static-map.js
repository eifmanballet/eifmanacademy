(function(window, document, $, undefined) {
    var defaults = {
        zoom: 15
    };

    $.StaticMap = function(root, options) {
        this.googleMap = undefined;

        this.init = function() {
            google.maps.event.addDomListener(window, 'load', this._initMap);
        };

        this._initMap = function() {
            var coords = new google.maps.LatLng(options.longitude, options.latitude);
            var placemark;

            this.googleMap = new google.maps.Map(root, {
                center: coords,
                zoom: options.zoom
            });

            placemark = new google.maps.Marker({
                position: coords
            });

            placemark.setMap(this.googleMap);
        };
    };

    $.fn.staticMap = function(options) {
        return this.each(function() {
            var map = this;
            var tabsData = $(map).data();
            var instanceOpts = $.extend(
                {},
                defaults,
                options,
                tabsData
            );

            return new $.StaticMap(map, instanceOpts).init();
        });
    };
})(window, document, jQuery, undefined);

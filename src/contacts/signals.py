# coding=utf-8
from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import loader
from contacts.models import Feedback


@receiver(post_save, sender=Feedback)
def send_contact_mail(sender, instance, **kwargs):
    # TODO: Вынести в settings
    subject = u"Сообщение с сайта eifmanacademy.ru"
    to = settings.CONTACT_EMAILS
    robot = settings.DEFAULT_SENDER
    context = {
        "message": instance
    }
    email_text = loader.render_to_string('email/contacts_form.txt', context)

    send_mail(subject, email_text, robot, to)

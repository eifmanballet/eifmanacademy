# coding=utf-8
from cms.app_base import CMSApp
from django.utils.translation import ugettext_lazy as _
from cms.apphook_pool import apphook_pool


@apphook_pool.register
class ContactsApp(CMSApp):
    name = _("Contacts App")
    urls = ["contacts.urls", ]
    app_name = 'contacts'

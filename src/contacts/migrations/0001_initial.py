# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AddressTranslation'
        db.create_table(u'contacts_address_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('address', self.gf('ckeditor.fields.RichTextField')()),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['contacts.Address'])),
        ))
        db.send_create_signal(u'contacts', ['AddressTranslation'])

        # Adding unique constraint on 'AddressTranslation', fields ['language_code', 'master']
        db.create_unique(u'contacts_address_translation', ['language_code', 'master_id'])

        # Adding model 'Address'
        db.create_table(u'contacts_address', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'contacts', ['Address'])

        # Adding model 'OfficialsTranslation'
        db.create_table(u'contacts_officials_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['contacts.Officials'])),
        ))
        db.send_create_signal(u'contacts', ['OfficialsTranslation'])

        # Adding unique constraint on 'OfficialsTranslation', fields ['language_code', 'master']
        db.create_unique(u'contacts_officials_translation', ['language_code', 'master_id'])

        # Adding model 'Officials'
        db.create_table(u'contacts_officials', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
        ))
        db.send_create_signal(u'contacts', ['Officials'])

        # Adding model 'Feedback'
        db.create_table(u'contacts_feedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('ip_address', self.gf('django.db.models.fields.IPAddressField')(default='0.0.0.0', max_length=15)),
        ))
        db.send_create_signal(u'contacts', ['Feedback'])


    def backwards(self, orm):
        # Removing unique constraint on 'OfficialsTranslation', fields ['language_code', 'master']
        db.delete_unique(u'contacts_officials_translation', ['language_code', 'master_id'])

        # Removing unique constraint on 'AddressTranslation', fields ['language_code', 'master']
        db.delete_unique(u'contacts_address_translation', ['language_code', 'master_id'])

        # Deleting model 'AddressTranslation'
        db.delete_table(u'contacts_address_translation')

        # Deleting model 'Address'
        db.delete_table(u'contacts_address')

        # Deleting model 'OfficialsTranslation'
        db.delete_table(u'contacts_officials_translation')

        # Deleting model 'Officials'
        db.delete_table(u'contacts_officials')

        # Deleting model 'Feedback'
        db.delete_table(u'contacts_feedback')


    models = {
        u'contacts.address': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Address'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'contacts.addresstranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'AddressTranslation', 'db_table': "u'contacts_address_translation'"},
            'address': ('ckeditor.fields.RichTextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['contacts.Address']"})
        },
        u'contacts.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'default': "'0.0.0.0'", 'max_length': '15'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contacts.officials': {
            'Meta': {'object_name': 'Officials'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        u'contacts.officialstranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'OfficialsTranslation', 'db_table': "u'contacts_officials_translation'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['contacts.Officials']"}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['contacts']
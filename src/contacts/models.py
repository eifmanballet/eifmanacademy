# coding=utf-8
from cms.models import CMSPlugin, PlaceholderField
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _
from django.db import models
from ckeditor.fields import RichTextField as HTMLField
from hvad.models import TranslatableModel, TranslatedFields
from model_utils.models import TimeStampedModel


class Address(TranslatableModel):
    translations = TranslatedFields(
        address=HTMLField(_("address")),
    )
    latitude = models.FloatField(_("latitude"), null=True, blank=True)
    longitude = models.FloatField(_("longitude"), null=True, blank=True)
    phone = models.CharField(_('phone number'), null=True, blank=True, max_length=20)
    email = models.EmailField(_('email'), null=True, blank=True)
    weight = models.PositiveIntegerField(_("weight"), null=True, blank=True)

    class Meta:
        verbose_name = _("address")
        verbose_name_plural = _("addresses")
        ordering = ['weight',
                    ]

    def __unicode__(self):
        address = self.lazy_translation_getter('address')
        return strip_tags(address)[:40] if address is not None else "..address.."


class Officials(TranslatableModel):
    translations = TranslatedFields(
        position=models.CharField(_("position"), null=True, blank=True, max_length=255),
    )
    phone = models.CharField(_('phone number'), null=True, blank=True, max_length=20)

    class Meta:
        verbose_name = _("official")
        verbose_name_plural = _("officials")

    def __unicode__(self):
        return self.safe_translation_getter('position')


class Feedback(TimeStampedModel):
    name = models.CharField(_('name'), max_length=255)
    email = models.EmailField(_('email'))
    phone = models.CharField(_('phone'), max_length=16, null=True, blank=True)
    subject = models.CharField(_('subject'), max_length=255, null=True, blank=True)
    message = models.TextField(_('message'))
    ip_address = models.IPAddressField(_('ip address'), default='0.0.0.0')

    class Meta:
        verbose_name = _("feedback")
        verbose_name_plural = _("feedbacks")

    def __unicode__(self):
        return u'{}... от {} ({})'.format(self.message, self.name, self.created)


class AddressPlugin(CMSPlugin):
    address = models.ForeignKey(Address)

    def __unicode__(self):
        return self.address

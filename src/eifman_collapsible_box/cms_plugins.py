# coding=utf-8
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from eifman_collapsible_box.models import CollapsibleBox


@plugin_pool.register_plugin
class CollapsibleBoxPlugin(CMSPluginBase):
    model = CollapsibleBox
    name = _("Collapsible box")
    render_template = "eifman_collapsible_box/collapsible_box.html"
    allow_children = True

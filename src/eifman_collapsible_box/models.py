# coding=utf-8
from django.db import models
from cms.models import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField


class CollapsibleBox(CMSPlugin):
    title = models.CharField(_("title"), max_length=255)
    content = HTMLField(_("content"), null=True, blank=True)
    is_collapsed = models.BooleanField(_("collapsed by default"), default=False)

    def __unicode__(self):
        return self.title

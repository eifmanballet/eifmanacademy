modules.define('i-bem__dom',
    function (provide, BEMDOM) {

        BEMDOM.decl('collapsible-box', {
            _toggle: function () {
                if (this.hasMod('collapsed')) {
                    this._show();
                } else {
                    this._hide();
                }
            },

            _show: function () {
                this.elem('content').stop().slideDown(function() {
                    this.delMod('collapsed');
                }.bind(this));
            },

            _hide: function () {
                this.elem('content').stop().slideUp(function() {
                    this.setMod('collapsed');
                }.bind(this));
            }
        }, {
            live: function() {
                this.liveBindTo('title', 'click', function () {
                    this._toggle();
                });
            }
        });

        provide(BEMDOM);
    });

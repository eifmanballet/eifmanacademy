# coding=utf-8
from django.db import models
from cms.models import CMSPlugin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

BOX_TYPES_DEFAULTS = [
    ('wide', _("Wide")),
    ('medium', _("Medium")),
    ('narrow', _("Narrow")),
]
BOX_TYPES = getattr(settings, 'EIFMAN_BOX_TYPES', BOX_TYPES_DEFAULTS)


class ContentBox(CMSPlugin):
    box_type = models.CharField(_("Box type"), max_length=64, choices=BOX_TYPES)
    custom_classes = models.CharField(_("custom classes"), max_length=255, blank=True, null=True)
    translatable_content_excluded_fields = ['custom_classes', 'box_type']

    def __unicode__(self):
        return self.box_type


class Belt(CMSPlugin):
    custom_classes = models.CharField(_("custom classes"), max_length=255, default='noop', blank=True, null=True)
    translatable_content_excluded_fields = ['custom_classes', ]

    def __unicode__(self):
        return _("Belt")

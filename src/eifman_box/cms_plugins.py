# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from eifman_box.models import ContentBox, Belt
from django.utils.translation import ugettext_lazy as _


@plugin_pool.register_plugin
class ContentBoxPlugin(CMSPluginBase):
    model = ContentBox
    name = _("Content box")
    module = "Academy"
    render_template = 'eifman_box/box.html'
    allow_children = True

    css_classes = {
        'wide': 'col-md-12',
        'medium': 'col-md-offset-2 col-md-8',
        'narrow': 'col-md-offset-3 col-md-6',
    }

    def _get_class_names(self, instance):
        return self.css_classes.get(instance.box_type, '')

    def render(self, context, instance, placeholder):
        class_names = self._get_class_names(instance)

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'class_names': class_names
        })

        return context


@plugin_pool.register_plugin
class BeltPlugin(CMSPluginBase):
    model = Belt
    name = _("Belt")
    module = "Academy"
    render_template = 'eifman_box/belt.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'placeholder': placeholder,
        })

        return context

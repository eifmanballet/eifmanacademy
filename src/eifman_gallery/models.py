from django.contrib.contenttypes import generic
from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer.fields.folder import FilerFolderField
from filer.fields.image import FilerImageField
from hvad.models import TranslatableModel, TranslatedFields
from model_utils.fields import AutoLastModifiedField
from model_utils.models import TimeStampedModel
from news.models import News
from youtubeurl_field.modelfields import YoutubeUrlField


class Album(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(_('title'), max_length=255)
    )
    created = models.DateTimeField(_('created'))
    modified = AutoLastModifiedField(_('modified'))
    news = generic.GenericRelation(News)
    folder = FilerFolderField(verbose_name=_('folder'))
    cover = FilerImageField(verbose_name=_('album cover'))

    class Meta:
        verbose_name = _("Album")
        verbose_name_plural = _("Albums")
        ordering = ['-created', ]

    def __unicode__(self):
        return self.lazy_translation_getter('title')

    @models.permalink
    def get_absolute_url(self):
        return ('gallery:gallery_album', (), {
            'pk': self.pk
        })


class Video(TranslatableModel, TimeStampedModel):
    translations = TranslatedFields(
        title=models.CharField(_('title'), max_length=255)
    )
    url = YoutubeUrlField('url', max_length=255)
    weight = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _("Video")
        verbose_name_plural = _("Videos")
        ordering = ('weight', '-created')

    def __unicode__(self):
        return self.lazy_translation_getter('title')

    @models.permalink
    def get_absolute_url(self):
        return ('gallery:gallery_album', (), {
            'pk': self.pk
        })

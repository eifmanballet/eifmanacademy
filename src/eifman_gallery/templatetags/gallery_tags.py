# coding=utf-8
from django import template
from eifman_gallery.models import Album, Video

register = template.Library()


@register.inclusion_tag('eifman_gallery/tags/albums.html')
def gallery_albums(count=8):

    return {
        "albums": Album.objects.all()[:count],
    }

@register.inclusion_tag('eifman_gallery/tags/videos.html')
def gallery_videos(count=3):

    return {
        "videos": Video.objects.all()[:count],
    }


from adminsortable.admin import SortableAdminMixin
from django.contrib import admin
from eifman_gallery.models import Album, Video
from hvad.admin import TranslatableAdmin


class GalleryAdmin(SortableAdminMixin, TranslatableAdmin):
    pass

admin.site.register(Album, TranslatableAdmin)
admin.site.register(Video, GalleryAdmin)

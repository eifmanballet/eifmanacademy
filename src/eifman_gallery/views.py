from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from eifman_gallery.models import Album, Video


class IndexView(TemplateView):
    template_name = 'eifman_gallery/gallery_index.html'
    pass


class AlbumsListView(ListView):
    model = Album


class VideosListView(ListView):
    model = Video


class AlbumDetailView(DetailView):
    model = Album

# coding=utf-8
from django.conf.urls import patterns, url
from eifman_gallery.views import AlbumsListView, IndexView, AlbumDetailView, VideosListView

urlpatterns = patterns('gallery',
   url(r'^$', IndexView.as_view(), name='gallery_index'),
   url(r'^albums$', AlbumsListView.as_view(), name='gallery_albums_list'),
   url(r'^videos$', VideosListView.as_view(), name='gallery_videos_list'),
   url(r'^albums/(?P<pk>\d+)$', AlbumDetailView.as_view(), name='gallery_album'),
)

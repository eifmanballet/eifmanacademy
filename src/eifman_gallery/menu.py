# coding=utf-8
from django.core.urlresolvers import reverse, resolve
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from eifman_gallery.models import Album


class CategoryMenu(CMSAttachMenu):
    name = _("Gallery menu")

    def _get_albums(self, request):
        nodes = []
        node = NavigationNode(_(u'видео'), reverse('gallery:gallery_videos_list'), 'gallery_videos')
        nodes.append(node)
        node = NavigationNode(_(u'альбомы'), reverse('gallery:gallery_albums_list'), 'gallery_albums')
        nodes.append(node)
        child_nodes = []

        for album in Album.objects.all():
            album_node = NavigationNode(album.lazy_translation_getter('title'),
                                        reverse('gallery:gallery_album', kwargs={'pk': album.pk}),
                                        album.pk, parent_id='gallery_albums')
            child_nodes.append(album_node)
        nodes += child_nodes

        return nodes

    def get_nodes(self, request):
        return self._get_albums(request)
        # return nodes


menu_pool.register_menu(CategoryMenu)

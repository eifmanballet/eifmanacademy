# coding=utf-8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from eifman_gallery.menu import CategoryMenu


@apphook_pool.register
class GalleryApp(CMSApp):
    name = _("Gallery App")
    urls = ["eifman_gallery.urls"]
    app_name = 'gallery'
    # menus = [CategoryMenu,]

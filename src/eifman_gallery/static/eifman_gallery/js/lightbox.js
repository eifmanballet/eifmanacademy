modules.define('i-bem__dom',
    function (provide, BEMDOM) {

        BEMDOM.decl('lightbox', {
            onSetMod: {
                js: {
                    inited: function() {
                        this.bindTo('item', 'click', this._itemClickHandler);
                        this._elemsCount = this.elem('item').length;
                    }
                }
            },

            _itemClickHandler: function(e) {
                var $elem = $(e.currentTarget);
                this._idx = $elem.data('id');

                this._$box = $('<div class="lightbox__shadow"><div class="lightbox__image-wrapper"><span class="lightbox__closer">×</span><img class="lightbox__image" src=""><div class="lightbox__arrows"><span class="lightbox__arrow lightbox__arrow_direction_prev">←</span><span class="lightbox__arrow lightbox__arrow_direction_next">→</span></div></div></div>').appendTo($('body'));
                this._$image = this.findElem(this._$box, 'image');
                this._$imageWrapper = this.findElem(this._$box, 'image-wrapper');

                this.bindTo(this.findElem(this._$box, 'arrow', 'direction', 'prev'), 'click', this._prev);
                this.bindTo(this.findElem(this._$box, 'arrow', 'direction', 'next'), 'click', this._next);
                this.bindTo(this.findElem(this._$box, 'closer'), 'click', this._hide);
                this.bindTo(this.findElem(this._$box, 'image'), 'click', this._next);
                this.bindTo(this.findElem(this._$box, 'shadow'), 'click', function(e) {
                    if (e.target == e.currentTarget) {
                        this._hide();
                    }
                });
                this._show();
            },

            _next: function() {
                if (this._idx < this._elemsCount) {
                    this._idx++;
                } else {
                    this._idx = 0;
                }

                this._show();
            },

            _prev: function() {
                if (this._idx > 0) {
                    this._idx--;
                } else {
                    this._idx = this._elemsCount - 1;
                }

                this._show();
            },

            _show: function() {
                var src = this.elem('item').eq(this._idx).data('src');
                this._$image.attr('src', src);
                this._$image.on('load', this._updatePosition.bind(this));
            },

            _hide: function() {
                this.unbindFrom(this.findElem(this._$box, 'arrow', 'direction', 'prev'), 'click');
                this.unbindFrom(this.findElem(this._$box, 'arrow', 'direction', 'next'), 'click');
                this.unbindFrom(this.findElem(this._$box, 'closer'), 'click');
                this._$box.remove();
                this._$box = null;
                this._$image = null;
                this._$imageWrapper = null;
            },

            _updatePosition: function() {
                var height = this._$imageWrapper.height(),
                    width = this._$imageWrapper.width();
                this._$imageWrapper.css({
                    'margin-left': -width/2,
                    'margin-top': -height/2
                });
            }
        }, {
        });

        provide(BEMDOM);
    }
);

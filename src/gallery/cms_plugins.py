# coding=utf-8
import json
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from gallery.models import GalleryPlugin


@plugin_pool.register_plugin
class EifmanGalleryPlugin(CMSPluginBase):
    module = "Academy"
    name = _("Gallery")
    model = GalleryPlugin
    text_enabled = False
    allow_children = True
    render_template = "gallery/gallery.html"

    @staticmethod
    def _get_script_params(instance):
        params = {
            'gallery': {
                'show_arrows': instance.show_arrows,
                'nav_options': instance.nav_options,
            },
        }

        return json.dumps(params)

    def render(self, context, instance, placeholder):
        js_params = self._get_script_params(instance)
        print(js_params)

        context.update({
            'instance': instance,
            'placeholder': placeholder,
            'js_params': js_params
        })
        return context

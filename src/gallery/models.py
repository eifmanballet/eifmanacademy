# coding=utf-8
from cms.models import CMSPlugin
from cmsplugin_filer_image.models import ThumbnailOption
from django.utils.translation import ugettext_lazy as _
from django.db import models

SHOW_ARROWS = (
    ('ALWAYS', _("Always")),
    ('WHEN_NEEDED', _("When needed")),
)

NAV_OPTION = (
    ('DOT', _("dots")),
    ('THUMBNAIL', _("thumbnails")),
    ('NONE', _("none")),
)


class GalleryPlugin(CMSPlugin):
    title = models.CharField(_("title"), max_length=255, null=True, blank=True)
    class_name = models.CharField(_("block class name"), max_length=255, blank=True, null=True)
    show_arrows = models.CharField(_("Show arrows"), max_length="64", choices=SHOW_ARROWS, default='WHEN_NEEDED')
    nav_options = models.CharField(_("nav options"), max_length="64", choices=NAV_OPTION, default='DOTS')

    def __unicode__(self):
        if self.title is not None:
            return self.title
        return "Gallery # {0}".format(self.pk)

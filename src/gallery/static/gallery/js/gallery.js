modules.define('i-bem__dom',
    function (provide, BEMDOM) {

        BEMDOM.decl('gallery', {
            _currentIndex: undefined,
            _itemsCount: undefined,

            onSetMod: {
                js: {
                    inited: function() {
                        this.bindTo('arrow', 'click', this._arrowClickHandler);
                        this._initNav();
                        this._showByIndex(0);
                    }
                }
            },

            _initNav: function() {
                if (! this.params.nav_options || this.params.nav_options === 'NONE') {
                    return;
                }

                this._itemsCount = this.elem('nav-item').length;

                if (this.params.nav_options === 'THUMBNAIL') {
                    this._initThumbsNav();
                }

                this.bindTo('nav-item', 'click', this._navClickHandler);
            },

            _initThumbsNav: function() {
                var thumbs = this.elem('thumbnail');

                this.elem('item').each(function(id, item) {
                    thumbs.eq(id).attr('src', $(item).find('img').attr('src'));
                });
            },

            _arrowClickHandler: function(e) {
                if (this.hasMod($(e.target), 'direction', 'left')) {
                    this._showPrev();
                }
                if (this.hasMod($(e.target), 'direction', 'right')) {
                    this._showNext();
                }
            },

            _showPrev: function() {
                if (this._currentIndex > 0) {
                    this._showByIndex(this._currentIndex - 1);
                }
            },

            _showNext: function() {
                if (this._currentIndex < this._itemsCount - 1) {
                    this._showByIndex(this._currentIndex + 1);
                }
            },

            _checkArrows: function () {
                this.delMod(this.elem('arrow'), 'disabled');

                if (this._currentIndex <= 0) {
                    this.setMod(this.elem('arrow', 'direction', 'left'), 'disabled');
                }

                if (this._currentIndex >= this._itemsCount - 1) {
                    this.setMod(this.elem('arrow', 'direction', 'right'), 'disabled');
                }
            },

            _showByIndex: function(idx) {
                this._currentIndex = idx;

                this.elem('wrapper').animate({
                    'left': '-' + this.elem('wrapper').width() * idx
                });

                this._selectNavItem(idx);
                this._checkArrows();
            },

            _navClickHandler: function(e) {
                var $target = $(e.currentTarget);

                this._showByIndex($target.data('index'));
            },

            _selectNavItem: function(idx) {
                this.delMod(this.elem('nav-item'), 'selected');
                this.setMod(this.elem('nav-item').eq(idx), 'selected');
            },

            getDefaultParams: function() {
                return {
                    nav_options: 'NONE',
                    show_arrows: 'WHEN_NEEDED'
                }
            }
        }, {});

        provide(BEMDOM);
    }
);

# coding=utf-8
from fabric.context_managers import shell_env
from fabric.contrib.files import append, exists, sed
from fabric.api import env, local, run, task
import random
from fabric.operations import sudo

USER = env.user
ENVIRON = getattr(env, 'environ', 'staging')
REPO_URL = 'git@bitbucket.org:eifmanballet/eifmanacademy.git'
SITES_DIR = '/home/{}/sites'.format(USER)
chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
ENV_PARAMS = {
    "SECRET_KEY": ''.join(random.SystemRandom().choice(chars) for _ in range(50)),
    "DJANGO_SETTINGS_MODULE": 'eifmanacademy.settings.{}'.format(ENVIRON),
}


def _create_project_dir_if_necessary(host):
    for subdir in ('htdocs', 'logs'):
        run('mkdir -p {}/{}/{}'.format(SITES_DIR, host, subdir))


def _get_latest_source(project_dir):
    if exists('{}/.git'.format(project_dir)):
        run('cd {} && git fetch'.format(project_dir))
    else:
        run('git clone {} {}'.format(REPO_URL, project_dir))
    current_commit = local("git log -n 1 --format=%H", capture=True)
    run('cd {} && git reset --hard {}'.format(project_dir, current_commit))


def _update_virtualenv(project_dir, environ):
    venv_dir = '{}/venv'.format(project_dir)
    if not exists('{}/bin/pip'.format(venv_dir)):
        run('virtualenv {}'.format(venv_dir))
    run('{}/bin/pip install -r {}/requirements/{}.txt'.format(venv_dir, project_dir, environ))


def _update_static_files(project_dir):
    with shell_env(**ENV_PARAMS):
        run('cd {} && ./venv/bin/python ./src/manage.py collectstatic --noinput'.format(project_dir))


def _update_database(source_folder):
    with shell_env(**ENV_PARAMS):
        run('cd {} && ./venv/bin/python ./src/manage.py syncdb --noinput'.format(source_folder))
        run('cd {} && ./venv/bin/python ./src/manage.py migrate --noinput'.format(source_folder))


def _update_project(project_dir, sitename):
    _create_project_dir_if_necessary(sitename)
    _get_latest_source(project_dir)
    _update_virtualenv(project_dir, ENVIRON)


def _restart_service(sitename):
    sudo('restart gunicorn-{}'.format(sitename))


@task
def deploy():
    sitename = env.host_string
    project_dir = '{}/{}/{}'.format(SITES_DIR, sitename, 'htdocs')
    _update_project(project_dir, sitename)
    _update_static_files(project_dir)
    _update_database(project_dir)


def _make_configuration(fname, **kwargs):
    f = open(fname)
    data = f.read()
    return data.format(**kwargs)


def _make_upstart_conf():
    return _make_configuration('deploy_tools/gunicorn-upstart.template.conf',
                               SITENAME=env.host_string,
                               SECRET_KEY=ENV_PARAMS['SECRET_KEY'],
                               DJANGO_SETTINGS_MODULE=ENV_PARAMS['DJANGO_SETTINGS_MODULE'],
                               PROJECTNAME=env.project, **globals())


def _make_nginx_conf():
    return _make_configuration('deploy_tools/nginx.template.conf', SITENAME=env.host_string, **globals())


@task
def setup_server():
    sitename = env.host_string
    project_dir = '{}/{}/{}'.format(SITES_DIR, sitename, 'htdocs')
    nginx_conf = _make_nginx_conf()
    upstart_conf = _make_upstart_conf()
    _update_project(project_dir, sitename)
    sudo('echo "{}" > /etc/nginx/sites-available/{}.conf'.format(nginx_conf, sitename))
    sudo('echo "{}" > /etc/init/gunicorn-{}.conf'.format(upstart_conf, sitename))
    sudo('ln -sf /etc/nginx/sites-available/{0}.conf /etc/nginx/sites-enabled/{0}.conf'.format(sitename))
    sudo('service nginx reload', pty=False)
    sudo('start gunicorn-{}'.format(sitename))

@task
def restart():
    sitename = env.host_string
    _restart_service(sitename)

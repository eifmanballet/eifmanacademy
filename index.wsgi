import os
import sys
from datetime import datetime

root_path = os.path.abspath(os.path.split(__file__)[0])


f = open(os.path.join(root_path, 'wsgi.log'), 'a')


try:
    sys.path.insert(0, os.path.join(root_path, 'venv/lib/python2.7/site-packages/'))
    sys.path.insert(0, os.path.join(root_path, 'venv/src/google-analytics/'))
    sys.path.insert(0, os.path.join(root_path, 'venv/src/sociallinks/'))
    sys.path.insert(0, os.path.join(root_path, 'venv/src/news/'))
    sys.path.insert(0, os.path.join(root_path, 'src/'))

    os.environ['SECRET_KEY'] = 'eifmanballetsettingsproduction'
    os.environ['DJANGO_SETTINGS_MODULE'] = 'eifmanacademy.settings.production'


    import django
    f.write(django.get_version())
    f.write('\n\n')
    f.write(str(sys.path))

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
except Exception as e:
    f.write("%s %s" % (datetime.now(), e.message))
    f.write("\n")

f.write("Loaded at {}\n".format(datetime.now()))
f.close()

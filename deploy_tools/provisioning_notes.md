# Развёртывание нового проекта

## Зависимости

* nginx
* python 2.7
* git
* pip
* virtualenv

На Ubuntu:

    sudo apt-get install nginx git python python-pip
    sudo pip install virtualenv
    
## Настройка nginx

* См. “nginx.template.conf
* Заменить SITENAME на, например, staging.my-domain.com
* Заменить USERNAME на имя нужного пользователя

## Настройка  Upstart

“## Upstart Job

* См. gunicorn-upstart.template.conf
* Заменить SITENAME на, например, staging.my-domain.com
* Заменить USERNAME на имя нужного пользователя
* Заменить PROJECTNAME на навзание джанго-проекта

## Файловая структура

Ожидаемая структура каталогов /home/username

/home/username
└── sites
    └── SITENAME
         ├── database
         ├── media
         ├── source
         ├── static
         └── venv
